program MultiTab;

uses
  Vcl.Forms,
  WEBLib.Forms,
  FMain in 'FMain.pas' {frmMain: TWebForm} {*.html},
  FDepartment in 'FDepartment.pas' {frmDepartment: TWebForm} {*.html},
  FEmployee in 'FEmployee.pas' {frmEmployee: TWebForm} {*.html};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
