unit FMain;

interface

uses
  System.SysUtils, System.Classes, JS, Web, WEBLib.Graphics, WEBLib.Controls,
  WEBLib.Forms, WEBLib.Dialogs, Vcl.Controls, WEBLib.ComCtrls, Vcl.Menus,
  WEBLib.Menus, WEBLib.ExtCtrls, WEBLib.Actions;

type
  TfrmMain = class(TWebForm)
    menMain: TWebMainMenu;
    menFile: TMenuItem;
    menDepartment: TMenuItem;
    menEmployee: TMenuItem;
    pgcMain: TWebPageControl;
    N1: TMenuItem;
    menExit: TMenuItem;
    pnlMainMenu: TWebPanel;
    menPopup: TWebPopupMenu;
    menClose: TMenuItem;
    procedure menExitClick(Sender: TObject);
    procedure menDepartmentClick(Sender: TObject);
    procedure menEmployeeClick(Sender: TObject);
    procedure menPopupPopup(Sender: TObject);
    procedure menCloseClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

uses
  WEBLib.ExtCtrls,
  FDepartment,
  FEmployee;

{$R *.dfm}

procedure TfrmMain.menCloseClick(Sender: TObject);
begin
  if pgcMain.PageCount > 0 then
    pgcMain.Items.Delete(pgcMain.ActivePageIndex);
end;

procedure TfrmMain.menDepartmentClick(Sender: TObject);
  procedure AfterCreate(AForm: TObject);
  begin
    (AForm as TfrmDepartment).lblDepartment.Caption := 'Department - ' + TimeToStr(Time);
  end;
var
  Index: Integer;
begin
  Index := pgcMain.Items.Add('Department');
  pgcMain.ActivePageIndex := Index;
  TfrmDepartment.CreateNew(pgcMain.ActivePage.ElementID, @AfterCreate);
end;

procedure TfrmMain.menEmployeeClick(Sender: TObject);
  procedure AfterCreate(AForm: TObject);
  begin
    (AForm as TfrmEmployee).lblEmployee.Caption := 'Employee - ' + TimeToStr(Time);
  end;
var
  Index: Integer;
begin
  Index := pgcMain.Items.Add('Employee');
  pgcMain.ActivePageIndex := Index;
  TfrmEmployee.CreateNew(pgcMain.ActivePage.ElementID, @AfterCreate);
end;

procedure TfrmMain.menExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.menPopupPopup(Sender: TObject);
begin
  menClose.Enabled := (pgcMain.PageCount > 0);
end;

end.
