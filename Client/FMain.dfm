object frmMain: TfrmMain
  Width = 955
  Height = 621
  Menu = menMain
  object pgcMain: TWebPageControl
    Left = 0
    Top = 33
    Width = 955
    Height = 568
    HeightPercent = 100.000000000000000000
    WidthPercent = 100.000000000000000000
    Align = alClient
    PopupMenu = menPopup
    TabOrder = 0
    ExplicitTop = 56
    ExplicitHeight = 545
  end
  object pnlMainMenu: TWebPanel
    Left = 0
    Top = 0
    Width = 955
    Height = 33
    HeightPercent = 100.000000000000000000
    WidthPercent = 100.000000000000000000
    Align = alTop
    BorderColor = clSilver
    BorderStyle = bsSingle
    ChildOrder = 1
  end
  object menMain: TWebMainMenu
    Appearance.HamburgerMenu.Caption = 'Menu'
    Appearance.SubmenuIndicator = '&#9658;'
    Container = pnlMainMenu
    Left = 88
    Top = 56
    object menFile: TMenuItem
      Caption = '&File'
      object menDepartment: TMenuItem
        Caption = 'Department'
        OnClick = menDepartmentClick
      end
      object menEmployee: TMenuItem
        Caption = 'Employee'
        OnClick = menEmployeeClick
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object menExit: TMenuItem
        Caption = 'Exit'
        OnClick = menExitClick
      end
    end
  end
  object menPopup: TWebPopupMenu
    Appearance.HamburgerMenu.Caption = 'Menu'
    Appearance.SubmenuIndicator = '&#9658;'
    Left = 384
    Top = 265
    object menClose: TMenuItem
      Caption = 'Close'
      OnClick = menCloseClick
    end
  end
end
